# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.4.0](https://gitlab.com/guardianproject-ops/ansible-docker-host/compare/0.3.1...0.4.0) (2022-02-01)


### ⚠ BREAKING CHANGES

* Manage docker daemon configuration file

### Features

* Manage docker daemon configuration file ([6d76220](https://gitlab.com/guardianproject-ops/ansible-docker-host/commit/6d76220e1dd82ed71c698d1603e952dc5cdf055c))

### [0.3.1](https://gitlab.com/guardianproject-ops/ansible-docker-host/compare/0.3.0...0.3.1) (2021-12-17)


### Features

* optionally allow setting of docker's data-dir at install time ([a070497](https://gitlab.com/guardianproject-ops/ansible-docker-host/commit/a07049741e081e91632b61d2c299d708e8b0cf26))

## [0.3.0](https://gitlab.com/guardianproject-ops/ansible-docker-host/compare/0.2.0...0.3.0) (2020-12-21)


### ⚠ BREAKING CHANGES

* Drop support for debian stretch

### Features

* Drop support for debian stretch ([5faabf1](https://gitlab.com/guardianproject-ops/ansible-docker-host/commit/5faabf1c8b793d07f5e1d245524c5b78cde86eff))

## [0.2.0](https://gitlab.com/guardianproject-ops/ansible-docker-host/compare/0.1.0...0.2.0) (2020-12-18)


### ⚠ BREAKING CHANGES

* update default version of docker-compose from github

### Features

* update default version of docker-compose from github ([fbf6a34](https://gitlab.com/guardianproject-ops/ansible-docker-host/commit/fbf6a34adb7ddf7b027dc97695377c6cd6b75372))
