---
- name: Ensure old versions of Docker are not installed
  package:
    name:
      - docker
      - docker-engine
    state: absent

- name: Update apt cache
  apt: update_cache=yes

- name: Ensure dependencies are installed
  apt:
    name:
      - apt-transport-https
      - ca-certificates
      - gpg
      - gpg-agent
    state: present

- name: Add Docker apt key
  apt_key:
    url: https://download.docker.com/linux/ubuntu/gpg
    id: 9DC858229FC7DD38854AE2D88D81803C0EBFCD88
    state: present
  register: add_repository_key
  ignore_errors: "{{ docker_apt_ignore_key_error }}"

- name: Add Docker repository
  apt_repository:
    repo: "{{ docker_apt_repository }}"
    state: present
    update_cache: true

- name: Check data root
  stat:
    path: "{{ docker_data_root }}"
  when: docker_data_root is defined
  register: docker_data_root_stat

- name: Assert data root exists
  assert:
    that:
      - docker_data_root_stat.stat.isdir is defined and docker_data_root_stat.stat.isdir
    fail_msg: The docker_data_root {{ docker_data_root }} does not exist. It must exist.
  when: docker_data_root is defined

- name: Create config dir
  file:
    path: /etc/docker
    owner: root
    group: root
    state: directory
  when: not docker_manage_daemon_json_disabled

- name: Create docker config
  template:
    src: daemon.json.j2
    dest: /etc/docker/daemon.json
    owner: root
    group: root
    mode: 0644
  when: not docker_manage_daemon_json_disabled
  notify: restart docker

- name: Install Docker
  package:
    name: "{{ docker_package }}"
    state: "{{ docker_package_state }}"
  notify: restart docker

- name: Install python docker library
  package:
    name: python3-docker
    state: present

- name: Ensure Docker is started and enabled at boot
  service:
    name: docker
    state: "{{ docker_service_state }}"
    enabled: "{{ docker_service_enabled }}"

- name: Ensure handlers are notified now to avoid firewall conflicts
  meta: flush_handlers

- include_tasks: docker-compose-github.yml
  when: docker_install_compose_from_github | bool

- include_tasks: docker-compose-apt.yml
  when: not docker_install_compose_from_github|bool

- include_tasks: docker-users.yml
  when: docker_users is defined

- include_tasks: docker-clean.yml
  when: docker_clean | bool
