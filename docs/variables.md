## Role Variables

* `docker_edition`: `'ce'` - Edition can be one of: 'ce' (Community Edition) or 'ee' (Enterprise Edition).



* `docker_service_state`: `started` - 



* `docker_service_enabled`: `true` - 



* `docker_restart_handler_state`: `restarted` - 



* `docker_install_compose_from_github`: `false` - whether to install docker compose from github or apt



* `docker_compose_github_version`: `"1.27.4"` - the github version to install (only relevant when `docker_install_compose_from_github` is `true`)



* `docker_users`: `[]` - A list of users who will be added to the docker group.



* `docker_data_root`: `` - A path that will be configured to be the data root instead of the default /var/lib/docker



* `docker_log_driver`: `journald` - The docker daemon logging driver to use (see https://docs.docker.com/config/containers/logging/configure/)



* `docker_custom_daemon_options`: `{}` - Allows passing of arbitrary/unsupported configuration options to 'daemon.json'.



* `docker_manage_daemon_json_disabled`: `false` - if set to true, this role will not touch the /etc/docker/daemon.json file


